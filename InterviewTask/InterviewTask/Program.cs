﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;

namespace InterviewTask
{
    class Program
    {
        public class FinancialDataItem
        {
            public DateTime Date { get; set; }
            public double Debit { get; set; }
            public double Credit { get; set; }
            public string PaidTo { get; set; }
            public string PaidBy { get; set; }
            public string Description { get; set; }

        }
        public class GroupedPayment
        {
            public List<FinancialDataItem> Group { get; set; }
        }

        static void Main(string[] args)
        {
            //Program is expected to be automated so a command line argument is accepted as the file input. Argument is expected to be a full path.            
            if (args == null || args.Length == 0 || args.Length > 1)
            {
                Console.WriteLine("Please enter one argument! InterviewTaske.exe <filepath> is the expected input!");
                Console.ReadKey();
                Environment.Exit(0);
            }
            List<FinancialDataItem> data = ImportFinancialData(args[0]);

            Console.WriteLine("--------------------------------");
            List<FinancialDataItem> possibleDuplicates = GetPossibleDuplicates(data);

            Console.WriteLine("--------------------------------");
            List<GroupedPayment> groupedPaymentsList = GetGroupedExpenditures(data);

            Console.WriteLine("--------------------------------");
            Console.ReadKey();
        }

        //Task 1 - This function imports data from a tab delimited txt file into a list of Financial Data
        //         The function takes a console input of a file location 
        public static List<FinancialDataItem> ImportFinancialData(string file)
        {
            var financialData = new List<FinancialDataItem>();

            Console.WriteLine("TASK 1:");

            //Get input of file if multiple files are required
            Console.WriteLine("Please input the location of the file you would like to import:");

            //Validating if file exists
            while (!File.Exists(file))
            {
                Console.WriteLine("The file is not valid. Please enter a new file!");
                file = Console.ReadLine();
            }

            //Iterate through each line, skipping the first as it's the column headers
            foreach (var line in File.ReadLines(file).Skip(1))
            {
                //Create a new FinancialDataItem for each row
                var dataLine = line.Split('\t');
                FinancialDataItem financialDataItem = new FinancialDataItem();

                //Convert string to Datetime
                financialDataItem.Date = Convert.ToDateTime(dataLine[0]);

                //This is another line that I am adding
                Console.WriteLine("Another Line");

                //Check if Debit is null. If null initialise as 0
                if (string.IsNullOrEmpty(dataLine[1]))
                {
                    financialDataItem.Debit = 0;
                }
                else
                {
                    financialDataItem.Debit = double.Parse(dataLine[1], NumberStyles.Currency);
                }

                //Check if credit is null. If null initialise as 0
                if (string.IsNullOrEmpty(dataLine[2]))
                {
                    financialDataItem.Credit = 0;
                }
                else
                {
                    financialDataItem.Credit = double.Parse(dataLine[2], NumberStyles.Currency);
                }

                financialDataItem.PaidTo = dataLine[3];
                financialDataItem.PaidBy = dataLine[4];
                financialDataItem.Description = dataLine[5];


                // Add the FinancialDataItem to the list of financial data
                financialData.Add(financialDataItem);
            }

            //Separate credits and debits
            var credits = financialData.Where(x => x.Credit != 0);
            var debits = financialData.Where(x => x.Debit != 0);

            //Calculate and Print total credit
            var totalCredit = credits.Sum(x => x.Credit);
            Console.WriteLine("- CREDITS TOTAL: $" + totalCredit);
            foreach (var credit in credits)
            {
                Console.WriteLine("-- " + credit.Date + " : RECEIVED $" + string.Format("{0:0.00}", credit.Credit) + " from " + credit.PaidBy + " - " + credit.Description);
            }

            //Calculate and print total debit
            var totalDebit = debits.Sum(x => x.Debit);

            //Print out debits
            Console.WriteLine("- DEBITS TOTAL: -$" + totalDebit);
            foreach (var debit in debits)
            {
                Console.WriteLine("-- " + debit.Date + " : PAID $" + string.Format("{0:0.00}", debit.Debit) + " to " + debit.PaidTo);
            }
            return financialData;
        }
        //Task 2 - This function filters the imported data into possible duplicates. The duplicates
        //         are calculated by same debit, date (within 20 seconds) and description
        public static List<FinancialDataItem> GetPossibleDuplicates(List<FinancialDataItem> data)
        {
            List<FinancialDataItem> possibleDuplicates = new List<FinancialDataItem>();


            Console.WriteLine("TASK 2:");
            Console.WriteLine("- Possible Duplicates");

            //Filter the data into debits that have the same amount, date and description
            var sameCostAndDate = data
                .GroupBy(i => new { i.Debit, Date = i.Date.ToString("dd/MM/yyyy"), i.Description })
                .Where(g => g.Count() > 1)
                .SelectMany(g => g).ToList();

            //Filter the data even more to only include debits made within 20 seconds of each other
            foreach (var possibleDuplicate in sameCostAndDate)
            {
                for (int i = 0; i < sameCostAndDate.Count(); i++)
                {
                    if ((Math.Abs((possibleDuplicate.Date - sameCostAndDate[i].Date).TotalSeconds) < 20) && possibleDuplicate != sameCostAndDate[i])
                    {
                        if (!possibleDuplicates.Contains(possibleDuplicate))
                        {
                            possibleDuplicates.Add(possibleDuplicate);
                        }
                        if (!possibleDuplicates.Contains(sameCostAndDate[i]))
                        {
                            possibleDuplicates.Add(sameCostAndDate[i]);
                        }

                    }
                }
            }
            //Print Out possible duplicates
            foreach (var duplicate in possibleDuplicates)
            {
                Console.WriteLine("-- " + duplicate.Date.ToString() + " : PAID $" + string.Format("{0:0.00}", duplicate.Debit) + " to " + duplicate.PaidTo);
            }
            return possibleDuplicates;

        }
        //Task 3 - 
        public static List<GroupedPayment> GetGroupedExpenditures(List<FinancialDataItem> data)
        {
            List<GroupedPayment> groupedPayments = new List<GroupedPayment>();

            Console.WriteLine("TASK 3:");
            Console.WriteLine("- Grouped Payments");

            //Filter out only debits
            var debits = data.Where(x => x.Debit != 0);

            //Group debits by "Paid To" field in order of total debits
            var rankedDebitList = debits
                .GroupBy(x => x.PaidTo)
                .Select(x => x.ToList())
                .OrderByDescending(x => x.Sum(y => y.Debit))
                .ToList();

            //Print out ranked debit list
            for (int i = 0; i < rankedDebitList.Count(); i++)
            {
                double total = rankedDebitList[i].Sum(item => item.Debit);
                Console.WriteLine("--" + (i + 1) + ": $" + string.Format("{0:0.00}", total) + " in " + rankedDebitList[i].Count() + " payments to " + rankedDebitList[i][0].PaidTo);
            }

            //Return data
            foreach (List<FinancialDataItem> group in rankedDebitList)
            {
                GroupedPayment groupedPayment = new GroupedPayment
                {
                    Group = group
                };
                groupedPayments.Add(groupedPayment);
            }
            return groupedPayments;
        }

    }
}
