# Read ME

This program is intended to be run through the command line accepting the first argument as the file path of the dataset.

First navigate to the folder of the exe:

`cd <path-to-folder>\msa-interviewtask-nov2020\InterviewTask\InterviewTask\bin\Debug`

Then run the .exe with the file path as the first argument:

`InterviewTask.exe <file-path>`